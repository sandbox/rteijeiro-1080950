<?php // $Id: metalmaker.admin.inc $

/**
 * Returns the metalmaker configuration form
 *
 * @return void
 * @author fran
 */
function _metalmaker_admin_form(&$form_state) {

  // Set the form page title  

  $title = t('MetalMaker Album and Song Generation');
  drupal_set_title($title);
  
  // Create the form

  $form = array();

  // Set the type of content you want to generate (Album or Song)
  
  $types = drupal_map_assoc(array(t('Album'), t('Song')));
  $type = !empty($form_state['values']['metalmaker_type']) ? $form_state['values']['metalmaker_type'] : t('Album');
  
  $form['metalmaker_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#default_value' => $type,
    '#options' => $types,
    '#description' => t('Select the content you want to generate'),
    '#ahah' => array(
      'path' => 'admin/settings/metalmaker/callback',
      'wrapper' => 'metalmaker-fieldset',
      'effect' => 'fade',
    )
  );

  // Fieldset with Album or Songs options. Loaded by AHAH callback
  $form['metalmaker_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('!type settings', array('!type' => $type)),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="metalmaker-fieldset">',
    '#suffix' => '</div>',
  );

  $nodetypes = metalmaker_get_nodetypes();

  $form['metalmaker_settings']['metalmaker_nodetype'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#default_value' => array('page' => 'Page'),
    '#options' => $nodetypes,
    '#description' => t('Select the Node Type you want for the generated Songs'),
  );

  $vocabularies = metalmaker_get_vocabularies();

  $form['metalmaker_settings']['metalmaker_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#default_value' => array('albums' => 'Albums'),
    '#options' => $vocabularies,
    '#description' => t('Select the Taxonomy Vocabulary for the Album terms'),
  );
  
  $form['metalmaker_settings']['metalmaker_title'] = array(
    '#type' => 'textfield',
    '#title' => t('!type title', array('!type' => $type)),
    '#default_value' => variable_get('metalmaker_title', ''),
    '#description' => t('Set the title of the !type. Leave it empty for random title.', array('!type' => $type)),
  );
  
  $form['metalmaker_settings']['metalmaker_instrumental_probability'] = array(
    '#type' => 'textfield',
    '#title' => t('Instrumental Song Probability'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => variable_get('metalmaker_instrumental_probability', 0),
    '#description' => t('Set the probability for a Song to be instrumental. Value from 0 to 100.'),
  );

  $tense = variable_get('metalmaker_tense', 'Undefined');
  $tenses = drupal_map_assoc(array(t('Undefined'), t('Present Participle'), t('Simple Past'), t('Past Participle')));

  $form['metalmaker_settings']['metalmaker_tense'] = array(
    '#type' => 'select',
    '#title' => t('!type Tense', array('!type' => $type)),
    '#default_value' => $tense,
    '#options' => $tenses,
    '#description' => t('Set the tense to be used in !type generation', array('!type' => $type)),
  );

  if ($tense == 'Undefined') {

    $form['metalmaker_settings']['metalmaker_tense_alteration'] = array(
      '#type' => 'textfield',
      '#title' => t('Tense Alteration'),
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => variable_get('metalmaker_tense_alteration', 0),
      '#description' => t('Set the probability for tense alteration between verses. Value from 0 to 100.'),
    );
  }

  // Album fields
  if ($type == 'Album') {

    $form['metalmaker_settings']['metalmaker_title_probability'] = array(
      '#type' => 'textfield',
      '#title' => t('Title Probability'),
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => variable_get('metalmaker_title_probability', 0),
      '#description' => t('Set the probability for Album title occurring also as Song title. Value from 0 to 100.'),
    );

    $form['metalmaker_settings']['metalmaker_song_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Song Count'),
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => variable_get('metalmaker_song_count', 0),
      '#description' => t('Set the number of Songs to be used in Album generation. If the value is set to zero a random amount of songs will be created. Value from 0 to 100.'),
    );

  }

  $form['metalmaker_settings']['metalmaker_generate'] = array(
    '#type' => 'submit',
    '#value' => t('Generate !type', array('!type' => $type)),
  );

  return $form;
}


/**
* Settings form validation for metalmaker module
* @param form an array that contains user data
* @param form_state an array that contains user data
*/
function _metalmaker_admin_form_validate(&$form, &$form_state) {

  $title_probability = $form_state['values']['metalmaker_title_probability'];

  if (is_numeric($title_probability)) {
    if (($title_probability < 0) || ($title_probability > 100)) {
      form_set_error('metalmaker_title_probability', t('Title probability value must be between 0 and 100.'));
    }
  }

  $instrumental_probability = $form_state['values']['metalmaker_instrumental_probability'];

  if (is_numeric($instrumental_probability)) {
    if (($instrumental_probability < 0) || ($instrumental_probability > 100)) {
      form_set_error('metalmaker_instrumental_probability', t('Instrumental probability value must be between 0 and 100.'));
    }
  }

  $song_count = $form_state['values']['metalmaker_song_count'];

  if (is_numeric($song_count)) {
    if (($song_count < 0) || ($song_count > 100)) {
      form_set_error('metalmaker_song_count', t('Song count value must be between 0 and 100.'));
    }
  }

  $tense_alteration = $form_state['values']['metalmaker_tense_alteration'];

  if (is_numeric($tense_alteration)) {
    if (($tense_alteration < 0) || ($tense_alteration > 100)) {
      form_set_error('metalmaker_tense_alteration', t('Tense alteration probability value must be between 0 and 100.'));
    }
  }

} // function _metalmaker_admin_config_form_validate


/**
* Generate form submit for metalmaker module
* @param form an array that contains user data
* @param form_state an array that contains user data
*/
function _metalmaker_admin_form_submit($form, &$form_state) {

  if (!empty($form_state['ahah_submission'])) {
    return;
  }

  // Create the element to generate
  $element = array();

  $element['type'] = $form_state['values']['metalmaker_type'];
  $element['title'] = $form_state['values']['metalmaker_title'];
  $element['instrumental_probability'] = $form_state['values']['metalmaker_instrumental_probability'];
  $element['tense'] = $form_state['values']['metalmaker_tense'];
  $element['tense_alteration'] = $form_state['values']['metalmaker_tense_alteration'];
  $element['title_probability'] = $form_state['values']['metalmaker_title_probability'];
  $element['song_count'] = $form_state['values']['metalmaker_song_count'];
  $element['nodetype'] = $form_state['values']['metalmaker_nodetype'];
  $element['vid'] = $form_state['values']['metalmaker_vocabulary'];

  require_once('metalmaker.generate.inc');

  metalmaker_generate($element);

}


/**
* ahah form callback for metalmaker module
*/
function metalmaker_admin_form_callback() {

  $form = metalmaker_callback_helper();

  $fieldset = $form['metalmaker_settings'];

  // Remove the prefix/suffix wrapper so we don't double it up
  unset($fieldset['#prefix'], $fieldset['#suffix']);

  // Render the output
  $output = theme('status_messages');
  $output .= drupal_render($fieldset);

  // Final rendering callback
  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}


/**
* MetalMaker Get Content Types
* @return associative array of content types
*/

function metalmaker_get_nodetypes() {

  $node_types = node_get_types();

  $types = array();

  foreach ( $node_types as $k => $v ) {

    $types[$k] = $v->name;

  }

  return $types;

}


/**
* Get all taxonomy vocabularies
*/
function metalmaker_get_vocabularies() {

  $vocabularies = taxonomy_get_vocabularies();

  $taxonomy = array();

  foreach($vocabularies as $vocabulary) {

    $taxonomy[$vocabulary->vid] = $vocabulary->name;

  }

  return $taxonomy;

}
