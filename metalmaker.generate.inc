<?php
// $Id: metalmaker.generate.inc, v.1.0 15/07/2010 Exp $
/**
* @file
* MetalMaker Module WebService functions
*
*/  


/**
* Generate album terms and song nodes from metalmaker webservice call
*/

function metalmaker_generate($element = array()) {
 
  $params = array();
  $nodes = array();
  $operations = array();

  // Clear old Webservice settings
  metalmaker_ws_call('ClearSettings');

  // Set WebService Settings for Albums and Songs

  if ($element['title'] != '') {
    $params = array('Title' => $element['title']);
    metalmaker_ws_call('SetTitle', $params);
  }

  if ($element['instrumental_probability'] != 0) {
    $params = array('Probability' => $element['instrumental_probability']);
    metalmaker_ws_call('SetInstrumentalProbability', $params);
  }

  if ($element['tense'] != 'Undefined') {
    $params = array('Tense' => $element['tense']);
    metalmaker_ws_call('SetTense', $params);

    if ($element['tense_alteration'] != 0) {
      $params = array('Probability' => $element['tense_alteration']);
      metalmaker_ws_call('SetTenseAlterationProbability', $params);
    }
  }

  // Set WebService Settings for Albums

  if ($element['type'] == 'Album') { // Generate Album

    $params = array('RequestType' => $element['type']);
    metalmaker_ws_call('SetRequestType', $params);

    if ($element['song_count'] != 0) {
      $params = array('SongCount' => $element['song_count']);
      metalmaker_ws_call('SetSongCount', $params);
    }

    if ($element['title_probability'] != 0) {
      $params = array('Probability' => $element['title_probability']);
      metalmaker_ws_call('SetAlbumTitleAsSongTitleProbability', $params);
    }

  }
  else { // Generate Song

    // Set WebService Settings for Songs
    $params = array('RequestType' => $element['type']);
    metalmaker_ws_call('SetRequestType', $params);   
  }

  $result = metalmaker_ws_call('Generate');

  if ( $result->GenerateResult == FALSE ) { // There is a WebService Error
    drupal_set_message(t('<h2>Error</h2>Error while retrieving WebService result'), 'error');
  }
  else { // Gets the Webservice Results

    $xml = $result->GenerateResult;
    $qpxml = qp($xml);

    $album = $qpxml->find('Album')->attr('title');

    $songs = 0;

    // Parse the XML result with QueryPath Library
    foreach ($qpxml->find('Song') as $song) {

      $title = $song->attr('title');

      if ($song->attr('type') == 'instrumental') { // If song is instrumental
        $body = "<p>Instrumental</p>";
      }

      else { // The song has lyric

        $body = ''; 

        $chorus = 0;

        foreach ($song->find('Verse') as $verse) { // Get all verses

          if ($chorus) { // Close the bold tag for chorus
            $body .= '</strong></p>';
            $chorus = 0;
          }

          if ($verse->attr('type') == 'Chorus') { // Put the chorus output bold
            $body .= "<p><strong>";
            $chorus = 1;
          }

          foreach ($verse->find('Line') as $line) { // Put all verse lines in paragraphs
            $body .= '<p>' . $line->text() . '</p>';
          }
        }

        if ($chorus) { // Close the bold tag for chorus if needed
          $body .= '</strong></p>';
          $chorus = 0;
        }

      }
   
    // Get the taxonomy term id for albums
    $tid = metalmaker_get_tid($album, $element['vid']);

    $node = array(
      'title' => $title,
      'body' => $body,
      'type' => $element['nodetype'],
      'tid' => $tid,
    );

    $operations[] = array('metalmaker_songs_create', array($node));
    
    } // foreach ends here

    $batch = array(
      'operations' => $operations,
      'finished' => 'metalmaker_songs_finished',
      'file' => drupal_get_path('module', 'metalmaker') . '/metalmaker.generate.inc', 
    );

  } // Get the WebService Results if-else ends here

  if (!isset($batch)) {
    drupal_set_message('<h2>Error while generating songs</h2>', 'error');
  }
  else {
    batch_set($batch);
    batch_process('admin/settings/metalmaker/generate');
  }

} // function metalmaker_generate end


/**
* Batch Generate song nodes from metalmaker webservice result
*/

function metalmaker_songs_create($node, &$context) {

  // Store some result for post-processing in the finished callback.
  $context['results'][] = check_plain($node['title']);

  // Optional message displayed under the progressbar.
  $context['message'] = t('Loading @title', array('@title' => $node['title']));

  $newnode = new stdClass();

  $newnode->title = $node['title'];
  $newnode->body = $node['body'];
  $newnode->teaser = $node['body'];
  $newnode->type = $node['type'];
  $newnode->format = 2;
  $newnode->status = 1;
  $newnode->created = time();
  $newnode->changed = $newnode->created;
  $newnode->taxonomy = array($tid => taxonomy_get_term($node['tid']));

  global $user;

  $newnode->uid = $user->uid;

  $newnode = node_submit($newnode);

  node_save($newnode);

} // function metalmaker_songs_create


/**
* MetalMaker Batch Finished Callback
*/

function metalmaker_songs_finished($success, $results, $operations) {

  if ($success) {
    $message = count($results) . ' songs generated';
  }
  else { // An error occurred
    $error_operation = reset($operations);
    $message = 'An error occurred while processing ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);

}


/**
* MetalMaker WebService Function Call
*/

function metalmaker_ws_call($function, $params = array()) {

  $wsdl = 'http://metalmaker.net/metalmaker.asmx?WSDL';

  $client = new SoapClient($wsdl);

  try {

    $result = $client->__call($function, $params);

  } catch (SoapFault $fault){

    watchdog('metalmaker',t('<h2>MetalMaker WebService Error !num</h2><pre>!error on !func function</pre>', array('!num' => $fault->faultcode, '!error' => $fault->faultstring, '!func' => $function)));

  }

  return $result;

}


/**
* Generate taxonomy from metalmaker album title if it doesn't exist
* If album exists returns the tid of it
* returns album taxonomy term id (tid)
*/

function metalmaker_get_tid($album, $vid) {
  
  $sql = "SELECT tid FROM {term_data} WHERE vid = %d AND name = '%s'";

  $tid = db_result(db_query($sql, $vid, $album));

  if (!$tid) {

    $sql = "INSERT INTO {term_data} VALUES ('', %d, '%s', '', 0)";

    db_query($sql, $vid, $album);

    $tid = db_last_insert_id('{term_data}', 'tid');

    $parent = 0;

    $sql = "INSERT INTO {term_hierarchy} VALUES (%d, %d)";

    db_query($sql, $tid, $parent);

  }

  return $tid;

} // function metalmaker_get_tid

