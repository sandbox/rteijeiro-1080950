MetalMaker is a random Heavy Metal song lyrics and album titles generator.

This module imports randomly generated song lyrics as Drupal nodes assigning them a random album title as a taxonomy term.

This module calls the MetalMaker WebService functions from metalmaker.net, parses the result XML content and create one node by each song lyric and one taxonomy term by album.

You can set up some settings as number of generated songs, the probability of a song being instrumental even the song verbal tense.

It's a nice module for educational purpose and WebService testing with PHP SOAP. The code has been commented out in order to be simple and clear.


Are you ready for Metal ?




Credits go to Matias Ärje (punisher@metalmaker.net): Webservice author.




TODO:

- Add Drush integration
